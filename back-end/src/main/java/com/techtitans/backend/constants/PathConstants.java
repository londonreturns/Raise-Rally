package com.techtitans.backend.constants;

// This class contains constant paths used in the application.
public class PathConstants {
    // Path for company creation
    public static final String COMPANY = "/api/companies";

    // Path for getting by ID
    public static final String GET_BY_ID_PATH = "/{id}";

    // Path for getting by Email
    public static final String GET_BY_EMAIL_PATH = "/email/{email}";

    // Path for getting the list of companies
    public static final String GET_ALL_PATH = "/all";

    // Path for backer creation
    public static final String BACKER = "/api/backers";

    // Path for admin creation
    public static final String ADMIN = "/api/admin";

    // Path for benefit
    public static final String BENEFIT = "/api/benefits";

    // Path for category
    public static final String CATEGORY = "/api/categories";

    // Path for price
    public static final String PRICE = "/api/price";

    // Path for product
    public static final String PRODUCT = "/api/products";
}